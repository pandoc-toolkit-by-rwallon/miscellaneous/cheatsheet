---
portrait:
language:
package:
    -
header-includes:
    -
color:
    - name:
      type:
      value:
links-as-notes:
package:
institute-logo:
    -
institute-size:

fullline-title:

# Listing colors
lst:
    background:
    comment:
    identifier:
    keyword:
    string:
    showspaces:

title:
language:
ncols:
logo:

title-background-color:
title-frame-color:
title-color:
---
