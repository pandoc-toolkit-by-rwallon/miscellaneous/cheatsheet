###############################################################################
#            MAKEFILE FOR EASILY CREATING CHEATSHEETS WITH PANDOC             #
###############################################################################

##########
# SOURCE #
##########

# The name of the file containing the metadata of the document.
# It may be left blank, e.g., if these data are put in the Markdown source file.
METADATA =

# The name of the Markdown source file to build the document from.
# Its extension must NOT be present.
FILENAME =

#######################
# CONVERSION SETTINGS #
#######################

# The options to pass to Pandoc when converting from Markdown to HTML.
# See Pandoc's documentation for more details.
PANDOC_OPTS = --listings

###########
# TARGETS #
###########

# Builds the document from the Markdown source file.
$(FILENAME).pdf: cheatsheet.pandoc $(METADATA) $(FILENAME).md
	pandoc $(PANDOC_OPTS) --template cheatsheet.pandoc $(METADATA) $(FILENAME).md --output $(FILENAME).pdf
