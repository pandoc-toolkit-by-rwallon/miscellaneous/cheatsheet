# Changelog

This file describes the evolution of the *Cheatsheet* template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (TBA)
