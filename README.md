# Pandoc Template for Creating *Cheatsheets*

## Description

This project provides a template allowing an easy creation of *Cheatsheets*
in plain Markdown using [Pandoc](https://pandoc.org).
